# Respackopts
Respackopts provides resource packs with config menus\
By default it integrates with frex (canvas shaders, currently requires the latest development release) and provides a custom system for conditional resources\
An example for the frex/canvas integration can be found [here](https://gitlab.com/jfmods/respackopts/-/tree/master/run/resourcepacks/lumi)

# Using Respackopts
## Users
You will just need to install Respackopts. A menu button will appear besides all supported resourcepacks.\
You can get the latest debug build [here](https://gitlab.com/jfmods/respackopts/-/jobs/artifacts/master/raw/latest.jar?job=build_test) and the latest stable build [here](https://modrinth.com/mod/respackopts)
## Resource pack authors
You can head to the [wiki](https://gitlab.com/jfmods/respackopts/-/wikis/home) for details.