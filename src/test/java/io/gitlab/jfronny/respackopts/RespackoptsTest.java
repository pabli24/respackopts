package io.gitlab.jfronny.respackopts;

import io.gitlab.jfronny.respackopts.data.entry.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

import static io.gitlab.jfronny.respackopts.Respackopts.*;
import static io.gitlab.jfronny.respackopts.Respackopts.CONFIG_BRANCH;
import static org.junit.jupiter.api.Assertions.*;

class RespackoptsTest {
    private static final String testEntryName = "test";
    private static final String testEntry1Name = "test1";

    @BeforeAll
    static void initialize() {
        LOGGER.info("Expected error end");
        CONF_DIR = Paths.get("./test");
        assertDoesNotThrow(() -> Files.deleteIfExists(CONF_DIR.resolve(testEntry1Name + ".json")));
        assertDoesNotThrow(() -> Files.createDirectories(CONF_DIR));
        SAVE_ACTIONS.add(() -> LOGGER.info("Save"));
        LOGGER.info(CONF_DIR);
    }

    @AfterEach
    void reset() {
        CONFIG_BRANCH.clear();
        save();
        assertEquals(CONFIG_BRANCH.keySet().size(), 0);
    }

    @BeforeEach
    void initSingle() {
        CONFIG_BRANCH.put(testEntryName, new ConfigBranch());
        assertEquals(CONFIG_BRANCH.keySet().size(), 1);
    }

    @Test
    void saveLoadSimple() {
        CONFIG_BRANCH.get(testEntryName).add(testEntry1Name, new ConfigBooleanEntry(false));
        assertTrue(CONFIG_BRANCH.get(testEntryName).has(testEntry1Name));
        assertFalse((Boolean) CONFIG_BRANCH.get(testEntryName).get(testEntry1Name).getValue());
        save();
        CONFIG_BRANCH.remove(testEntryName);
        assertEquals(CONFIG_BRANCH.keySet().size(), 0);
        CONFIG_BRANCH.put(testEntryName, new ConfigBranch());
        CONFIG_BRANCH.get(testEntryName).add(testEntry1Name, new ConfigBooleanEntry(false));
        load(testEntryName);
        assertEquals(CONFIG_BRANCH.keySet().size(), 1);
        assertTrue(CONFIG_BRANCH.get(testEntryName).has(testEntry1Name));
        assertFalse((Boolean) CONFIG_BRANCH.get(testEntryName).get(testEntry1Name).getValue());
    }

    @Test
    void syncSimple() {
        ConfigBranch test = new ConfigBranch();
        test.add(testEntry1Name, new ConfigBooleanEntry(false));
        CONFIG_BRANCH.get(testEntryName).sync(test, SyncMode.RESPACK_LOAD);
        save();
        load(testEntryName);
        assertFalse((Boolean) CONFIG_BRANCH.get(testEntryName).get(testEntry1Name).getValue());
        assertEquals(CONFIG_BRANCH.keySet().size(), 1);
        assertEquals(test.getValue().size(), 1);
        assertNotNull(CONFIG_BRANCH.get(testEntryName));
        ConfigBooleanEntry be;
        assertTrue(test.get(testEntry1Name) instanceof ConfigBooleanEntry);
        be = (ConfigBooleanEntry)test.get(testEntry1Name);
        be.setValue(true);
        assertFalse((Boolean) CONFIG_BRANCH.get(testEntryName).get(testEntry1Name).getValue());
        LOGGER.info("E");
        CONFIG_BRANCH.get(testEntryName).sync(test, SyncMode.RESPACK_LOAD);
        assertFalse((Boolean) CONFIG_BRANCH.get(testEntryName).get(testEntry1Name).getValue());
        CONFIG_BRANCH.get(testEntryName).sync(test, SyncMode.CONF_LOAD);
        save();
        load(testEntryName);
        assertTrue((Boolean) CONFIG_BRANCH.get(testEntryName).get(testEntry1Name).getValue());
    }

    @Test
    void syncWrongType() {
        ConfigBranch cb = new ConfigBranch();
        ConfigBranch cbNew = new ConfigBranch();
        cb.add(testEntryName, new ConfigBooleanEntry(false));
        cbNew.add(testEntryName, new ConfigNumericEntry());
        LOGGER.info("Expecting warning message");
        cbNew.sync(cb, SyncMode.RESPACK_LOAD);
        LOGGER.info("Expected warning end");
        cbNew.add(testEntryName, new ConfigBooleanEntry(true));
        cbNew.sync(cb, SyncMode.RESPACK_LOAD);
    }
}
