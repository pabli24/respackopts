package io.gitlab.jfronny.respackopts.mixin;

import com.mojang.blaze3d.systems.RenderSystem;
import io.gitlab.jfronny.respackopts.Respackopts;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.pack.PackListWidget;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PackListWidget.ResourcePackEntry.class)
public abstract class ResourcePackEntryMixin {
    @Final @Shadow private PackListWidget widget;

    @Shadow protected abstract boolean isSelectable();

    @Shadow @Final private ResourcePackOrganizer.Pack pack;
    boolean rpo$selected;

    @Inject(at = @At("TAIL"), method = "render(Lnet/minecraft/client/util/math/MatrixStack;IIIIIIIZF)V")
    private void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta, CallbackInfo info) {
        if (this.isSelectable() && Respackopts.NAME_LOOKUP.containsKey(pack.getDisplayName().asString())) {
            int d = mouseX - x;
            d = widget.getRowWidth() - d;
            int e = mouseY - y;
            rpo$selected = d <= 32 && d >= 12 && e <= entryHeight / 2 + 10 && e >= entryHeight / 2 - 10;
            rpo$renderButton(matrices, rpo$selected, new Identifier("modmenu", "textures/gui/configure_button.png"), x + entryWidth - 30, y + entryHeight / 2 - 10);
        }
    }

    @Inject(at = @At("RETURN"), method = "mouseClicked(DDI)Z", cancellable = true)
    public void mouseClicked(double mouseX, double mouseY, int button, CallbackInfoReturnable<Boolean> info) {
        if (!info.getReturnValue()) {
            if (this.isSelectable()) {
                String k = pack.getDisplayName().asString();
                if (Respackopts.NAME_LOOKUP.containsKey(k) && rpo$selected) {
                    k = Respackopts.NAME_LOOKUP.get(k);
                    info.setReturnValue(true);
                    MinecraftClient c = MinecraftClient.getInstance();
                    c.openScreen(Respackopts.factory.buildGui(Respackopts.CONFIG_BRANCH.get(k), k, c.currentScreen, () -> Respackopts.forceRespackReload = true));
                }
            }
        }
    }

    private void rpo$renderButton(MatrixStack matrices, boolean hovered, Identifier texture, int x, int y) {
        RenderSystem.setShaderColor(1, 1, 1, 1f);
        RenderSystem.setShaderTexture(0, texture);
        RenderSystem.disableDepthTest();
        DrawableHelper.drawTexture(matrices, x, y, 0, hovered ? 20 : 0, 20, 20, 32, 64);
        RenderSystem.enableDepthTest();
    }
}
