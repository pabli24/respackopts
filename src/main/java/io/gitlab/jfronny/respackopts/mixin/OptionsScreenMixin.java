package io.gitlab.jfronny.respackopts.mixin;

import io.gitlab.jfronny.respackopts.Respackopts;
import net.minecraft.client.gui.screen.option.OptionsScreen;
import net.minecraft.client.option.GameOptions;
import net.minecraft.resource.ResourcePackManager;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(OptionsScreen.class)
public class OptionsScreenMixin {
    @Shadow @Final private GameOptions settings;

    @Inject(at = @At("HEAD"), method = "refreshResourcePacks(Lnet/minecraft/resource/ResourcePackManager;)V")
    private void refreshResourcePacks(ResourcePackManager resourcePackManager, CallbackInfo info) {
        if (Respackopts.forceRespackReload) {
            Respackopts.forceRespackReload = false;
            this.settings.resourcePacks.clear();
        }
    }
}
