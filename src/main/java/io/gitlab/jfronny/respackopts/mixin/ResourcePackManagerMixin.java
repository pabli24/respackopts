package io.gitlab.jfronny.respackopts.mixin;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.Respackmeta;
import io.gitlab.jfronny.respackopts.data.entry.SyncMode;
import net.minecraft.resource.ResourcePackManager;
import net.minecraft.resource.ResourcePackProfile;
import net.minecraft.resource.ResourceType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@Mixin(ResourcePackManager.class)
public class ResourcePackManagerMixin {
    @Shadow private Map<String, ResourcePackProfile> profiles;

    @Inject(at = @At("TAIL"), method = "scanPacks()V")
    private void scanPacks(CallbackInfo info) {
        Respackopts.NAME_LOOKUP.clear();
        profiles.forEach((s, v) -> {
            if (rpo$hasMetadata(v)) {
                try {
                    Respackmeta conf = Respackopts.GSON.fromJson(rpo$readMetadata(v, Respackopts.GSON), Respackmeta.class);
                    if (Respackopts.META_VERSION < conf.version) {
                        Respackopts.LOGGER.error(s + " was not loaded as it specifies a newer respackopts version than is installed");
                        return;
                    }
                    if (Respackopts.META_VERSION > conf.version) {
                        Respackopts.LOGGER.warn(s + " uses an outdated RPO format (" + conf.version + "). Although this is supported, using the latest version (" + Respackopts.META_VERSION + ") is recommended");
                    }
                    conf.conf.setVersion(conf.version);
                    if (!Respackopts.CONFIG_BRANCH.containsKey(conf.id))
                        Respackopts.CONFIG_BRANCH.put(conf.id, conf.conf);
                    else
                        Respackopts.CONFIG_BRANCH.get(conf.id).sync(conf.conf, SyncMode.RESPACK_LOAD);
                    Respackopts.NAME_LOOKUP.put(v.getDisplayName().asString(), conf.id);
                    Respackopts.load(conf.id);
                } catch (Throwable e) {
                    Respackopts.LOGGER.error("Could not initialize pack meta for " + s, e);
                }
            }
        });
    }

    private boolean rpo$hasMetadata(ResourcePackProfile v) {
        return v.createResourcePack().contains(ResourceType.CLIENT_RESOURCES, Respackopts.CONF_ID);
    }

    private JsonObject rpo$readMetadata(ResourcePackProfile v, Gson g) throws IOException {
        InputStream is = v.createResourcePack().open(ResourceType.CLIENT_RESOURCES, Respackopts.CONF_ID);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) != -1) {
            os.write(buffer, 0, length);
        }
        return g.fromJson(os.toString(), JsonElement.class).getAsJsonObject();
    }
}
