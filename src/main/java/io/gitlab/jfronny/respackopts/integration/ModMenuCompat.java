package io.gitlab.jfronny.respackopts.integration;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import io.gitlab.jfronny.respackopts.Respackopts;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.FatalErrorScreen;
import net.minecraft.text.TranslatableText;

import java.util.concurrent.atomic.AtomicBoolean;

public class ModMenuCompat implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> {
            try {
                ConfigBuilder builder;
                builder = ConfigBuilder.create()
                        .setParentScreen(parent)
                        .setTitle(new TranslatableText("respackopts.mainconfig"));
                ConfigEntryBuilder entryBuilder = builder.entryBuilder();
                builder.setSavingRunnable(() -> {
                    Respackopts.save();
                    MinecraftClient.getInstance().reloadResources();
                });
                AtomicBoolean categoryAdded = new AtomicBoolean(false);
                Respackopts.CONFIG_BRANCH.forEach((id, conf) -> {
                    categoryAdded.set(true);
                    ConfigCategory config = builder.getOrCreateCategory(new TranslatableText("respackopts.title." + id));
                    Respackopts.factory.buildCategory(conf, id, config::addEntry, entryBuilder, "");
                });
                if (!categoryAdded.get()) {
                    builder.getOrCreateCategory(new TranslatableText("respackopts.mainconfig"))
                        .addEntry(entryBuilder.startTextDescription(new TranslatableText("respackopts.noPack")).build());
                }
                return builder.build();
            }
            catch (Throwable t) {
                t.printStackTrace();
                return new FatalErrorScreen(new TranslatableText("respackopts.loadFailed"), new TranslatableText("respackopts.loadError"));
            }
        };
    }
}
