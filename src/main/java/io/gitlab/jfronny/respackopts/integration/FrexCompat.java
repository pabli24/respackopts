package io.gitlab.jfronny.respackopts.integration;

import grondag.frex.FrexInitializer;
import grondag.frex.api.config.ShaderConfig;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.entry.ConfigBranch;
import net.minecraft.util.Identifier;

import java.util.Map;

public class FrexCompat implements FrexInitializer {
    boolean initial = true;
    @Override
    public void onInitalizeFrex() {
        ShaderConfig.registerShaderConfigSupplier(new Identifier(Respackopts.ID, "config_supplier"), () -> {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, ConfigBranch> e : Respackopts.CONFIG_BRANCH.entrySet()) {
                e.getValue().buildShader(sb, Respackopts.sanitizeString(e.getKey()));
            }
            sb.append("\n#define respackopts_loaded");
            return sb.toString();
        });
        Respackopts.LOGGER.info("enabled frex/canvas support");
        Respackopts.SAVE_ACTIONS.add(() -> {
            try {
                if (!initial)
                    ShaderConfig.invalidateShaderConfig();
                initial = false;
            }
            catch (Throwable e) {
                Respackopts.LOGGER.error("Could not reload shader config", e);
            }
        });
    }
}
