package io.gitlab.jfronny.respackopts;

import io.gitlab.jfronny.respackopts.data.entry.ConfigBranch;
import io.gitlab.jfronny.respackopts.data.entry.ConfigEntry;
import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.client.gui.screen.FatalErrorScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Language;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public class GuiFactory {
    public void buildCategory(ConfigBranch source, String screenId, Consumer<AbstractConfigListEntry<?>> config, ConfigEntryBuilder entryBuilder, String namePrefix) {
        for (Map.Entry<String, ConfigEntry<?>> in : source.getValue().entrySet()) {
            ConfigEntry<?> entry = in.getValue();
            String entryName = ("".equals(namePrefix) ? "" : namePrefix + ".") + in.getKey();
            Respackopts.LOGGER.info(entryName);
            String translationPrefix = "respackopts." + entry.getEntryType() + "." + screenId;
            config.accept(entry.buildEntry(entryBuilder,
                    getText(entryName, translationPrefix),
                    () -> {
                        String k = "respackopts.tooltip." + screenId + "." + entryName;
                        if (Language.getInstance().hasTranslation(k)) {
                            Text[] res = new Text[1];
                            res[0] = new TranslatableText(k);
                            return Optional.of(res);
                        }
                        else
                            return Optional.empty();
                    },
                    screenId,
                    entryName,
                    translationPrefix));
        }
    }

    public static Text getText(String name, String translationPrefix) {
        String translatableNameKey = translationPrefix + "." + name;
        return Language.getInstance().hasTranslation(translatableNameKey)
                ? new TranslatableText(translatableNameKey)
                : new LiteralText(name);
    }

    public Screen buildGui(ConfigBranch source, String resourcepackid, Screen parent, Runnable onSave) {
        try {
            ConfigBuilder builder;
            builder = ConfigBuilder.create()
                    .setParentScreen(parent)
                    .setTitle(getText(resourcepackid, "respackopts.title"));
            ConfigEntryBuilder entryBuilder = builder.entryBuilder();
            builder.setSavingRunnable(() -> {
                Respackopts.save();
                onSave.run();
            });
            ConfigCategory config = builder.getOrCreateCategory(getText(resourcepackid, "respackopts.title"));
            buildCategory(source, resourcepackid, config::addEntry, entryBuilder, "");
            return builder.build();
        }
        catch (Throwable t) {
            t.printStackTrace();
            return new FatalErrorScreen(new TranslatableText("respackopts.loadFailed"), new TranslatableText("respackopts.loadError"));
        }
    }
}
