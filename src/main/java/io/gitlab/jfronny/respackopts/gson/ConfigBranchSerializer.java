package io.gitlab.jfronny.respackopts.gson;

import com.google.gson.*;
import io.gitlab.jfronny.respackopts.data.entry.*;

import java.lang.reflect.Type;
import java.util.Map;

public class ConfigBranchSerializer implements JsonSerializer<ConfigBranch>, JsonDeserializer<ConfigBranch> {
    @Override
    public JsonElement serialize(ConfigBranch src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject o = new JsonObject();
        for (Map.Entry<String, ConfigEntry<?>> entry : src.getValue().entrySet()) {
            o.add(entry.getKey(), context.serialize(entry.getValue()));
        }
        return o;
    }

    @Override
    public ConfigBranch deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (!json.isJsonObject())
            throw new JsonSyntaxException("ConfigBranch is not an object");
        JsonObject o = json.getAsJsonObject();
        if (o.entrySet().stream().allMatch(s -> "bools".equals(s.getKey()) || "doubles".equals(s.getKey()) || "strings".equals(s.getKey()))) {
            return LegacyConfigDeserializer.deserialize(json, context);
        }
        ConfigBranch cbNew = new ConfigBranch();
        for (Map.Entry<String, JsonElement> e : o.entrySet()) {
            JsonElement j = e.getValue();
            String s = e.getKey();
            if (NumericEntrySerializer.isSlider(j))
                cbNew.add(s, context.deserialize(j, ConfigNumericEntry.class));
            else if (j.isJsonPrimitive()) {
                JsonPrimitive p = j.getAsJsonPrimitive();
                if (p.isBoolean())
                    cbNew.add(s, new ConfigBooleanEntry(p.getAsBoolean()));
                else if (p.isNumber())
                    cbNew.add(s, context.deserialize(j, ConfigNumericEntry.class));
                else if (p.isString())
                    cbNew.add(s, context.deserialize(j, ConfigEnumEntry.class));
            }
            else if (j.isJsonArray())
                cbNew.add(s, context.deserialize(j, ConfigEnumEntry.class));
            else if (j.isJsonObject())
                cbNew.add(s, context.deserialize(j, ConfigBranch.class));
            else if (j.isJsonNull())
                throw new JsonSyntaxException("Unexpected json null");
            else
                throw new JsonSyntaxException("Unexpected json object type");
        }
        return cbNew;
    }
}
