package io.gitlab.jfronny.respackopts.gson;

import com.google.gson.*;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.entry.ConfigEnumEntry;

import java.lang.reflect.Type;

public class EnumEntrySerializer implements JsonSerializer<ConfigEnumEntry>, JsonDeserializer<ConfigEnumEntry> {
    @Override
    public JsonElement serialize(ConfigEnumEntry src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getValue());
    }

    @Override
    public ConfigEnumEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ConfigEnumEntry result = new ConfigEnumEntry();
        if (json.isJsonPrimitive()) {
            JsonPrimitive jp = json.getAsJsonPrimitive();
            if (jp.isString()) {
                result.setValue(jp.getAsString());
                return result;
            }
            else if (jp.isNumber()) {
                result.setNextValue(jp.getAsInt());
                return result;
            }
            else
                throw new JsonSyntaxException("Expected primitive string key for enum");
        }
        else if (json.isJsonArray()) {
            result.values.clear();
            for (JsonElement e : json.getAsJsonArray()) {
                if (e.isJsonPrimitive() && e.getAsJsonPrimitive().isString()) {
                    result.values.add(e.getAsString());
                }
                else
                    throw new JsonSyntaxException("Expected string entry in enum");
            }
            if (result.values.isEmpty())
                Respackopts.LOGGER.warn("Enum entry empty");
            else
                result.setDefault(result.values.get(0));
            return result;
        }
        else
            throw new JsonSyntaxException("Expected primitive string key or string array for enum");
    }
}
