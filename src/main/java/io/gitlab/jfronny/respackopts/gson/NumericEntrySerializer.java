package io.gitlab.jfronny.respackopts.gson;

import com.google.gson.*;
import io.gitlab.jfronny.respackopts.data.entry.ConfigNumericEntry;

import java.lang.reflect.Type;

public class NumericEntrySerializer implements JsonSerializer<ConfigNumericEntry>, JsonDeserializer<ConfigNumericEntry> {
    @Override
    public JsonElement serialize(ConfigNumericEntry src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getValue());
    }
    @Override
    public ConfigNumericEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ConfigNumericEntry result = new ConfigNumericEntry();
        if (json.isJsonPrimitive() && json.getAsJsonPrimitive().isNumber()) {
            result.setValue(json.getAsDouble());
            result.setDefault(json.getAsDouble());
            return result;
        }
        else if (isSlider(json)) {
            JsonObject o = json.getAsJsonObject();
            JsonElement def = o.get("default");
            JsonElement min = o.get("min");
            JsonElement max = o.get("max");
            if (def.isJsonPrimitive() && def.getAsJsonPrimitive().isNumber()
                    && min.isJsonPrimitive() && min.getAsJsonPrimitive().isNumber()
                    && max.isJsonPrimitive() && max.getAsJsonPrimitive().isNumber()) {
                double defV = def.getAsNumber().doubleValue();
                double minV = min.getAsNumber().doubleValue();
                double maxV = max.getAsNumber().doubleValue();
                if (defV < minV || defV > maxV) {
                    throw new JsonSyntaxException("Default value out of range in slider definition");
                }
                if (isWhole(defV) && isWhole(minV) && isWhole(maxV)) {
                    result.setValue(defV);
                    result.setDefault(defV);
                    result.min = minV;
                    result.max = maxV;
                    return result;
                }
                throw new JsonSyntaxException("Expected whole number in slider definition");
            }
            throw new JsonSyntaxException("Expected numeric values in slider definition");
        }
        throw new JsonSyntaxException("Could not deserialize numeric entry");
    }
    
    public static boolean isSlider(JsonElement element) {
        return element.isJsonObject() && element.getAsJsonObject().entrySet().stream().allMatch(s -> "default".equals(s.getKey()) || "min".equals(s.getKey()) || "max".equals(s.getKey()));
    }

    private boolean isWhole(double v) {
        return v == Math.floor(v) && !Double.isInfinite(v);
    }
}
