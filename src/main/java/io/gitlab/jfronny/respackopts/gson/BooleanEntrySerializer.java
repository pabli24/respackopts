package io.gitlab.jfronny.respackopts.gson;

import com.google.gson.*;
import io.gitlab.jfronny.respackopts.data.entry.ConfigBooleanEntry;

import java.lang.reflect.Type;

public class BooleanEntrySerializer implements JsonSerializer<ConfigBooleanEntry>, JsonDeserializer<ConfigBooleanEntry> {
    @Override
    public ConfigBooleanEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new ConfigBooleanEntry(json.getAsBoolean());
    }

    @Override
    public JsonElement serialize(ConfigBooleanEntry src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getValue());
    }
}
