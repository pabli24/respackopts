package io.gitlab.jfronny.respackopts.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import io.gitlab.jfronny.respackopts.data.LegacyConfig;
import io.gitlab.jfronny.respackopts.data.entry.ConfigBooleanEntry;
import io.gitlab.jfronny.respackopts.data.entry.ConfigBranch;
import io.gitlab.jfronny.respackopts.data.entry.ConfigNumericEntry;

import java.util.Map;

public class LegacyConfigDeserializer {
    public static ConfigBranch deserialize(JsonElement json, JsonDeserializationContext context) throws JsonParseException {
        ConfigBranch cb = new ConfigBranch();
        LegacyConfig lc = context.deserialize(json, LegacyConfig.class);
        for (Map.Entry<String, Boolean> e : lc.bools.entrySet()) {
            ConfigBooleanEntry c = new ConfigBooleanEntry(e.getValue());
            int i = e.getKey().lastIndexOf('.');
            if (i == -1)
                cb.add(e.getKey(), c);
            else {
                cb = getBranch(e.getKey().substring(0, i), cb);
                String n = e.getKey().substring(i + 1);
                cb.add(n, c);
            }
        }
        for (Map.Entry<String, Double> e : lc.doubles.entrySet()) {
            ConfigNumericEntry ne = new ConfigNumericEntry();
            ne.setValue(e.getValue());
            ne.setDefault(e.getValue());
            int i = e.getKey().lastIndexOf('.');
            if (i == -1)
                cb.add(e.getKey(), ne);
            else
                getBranch(e.getKey().substring(0, i), cb).add(e.getKey().substring(i + 1), ne);
        }
        return cb;
    }
    
    private static ConfigBranch getBranch(String name, ConfigBranch base) {
        int i = name.indexOf('.');
        String keyNext = i == -1 ? name : name.substring(0, i);
        if (!base.has(keyNext))
            base.add(keyNext, new ConfigBranch());
        ConfigBranch c = (ConfigBranch)base.get(keyNext);
        return i == -1 ? c : getBranch(name.substring(i + 1), c);
    }
}
