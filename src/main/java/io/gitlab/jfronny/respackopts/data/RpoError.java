package io.gitlab.jfronny.respackopts.data;

public class RpoError extends Exception {
    public RpoError(String message) {
        super(message);
    }
}
