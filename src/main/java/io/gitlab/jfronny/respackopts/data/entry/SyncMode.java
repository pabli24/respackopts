package io.gitlab.jfronny.respackopts.data.entry;

public enum SyncMode {
    RESPACK_LOAD, CONF_LOAD
}
