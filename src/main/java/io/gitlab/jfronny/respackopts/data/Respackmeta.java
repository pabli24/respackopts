package io.gitlab.jfronny.respackopts.data;

import io.gitlab.jfronny.respackopts.data.entry.ConfigBranch;

public class Respackmeta {
    public ConfigBranch conf;
    public String id;
    public Integer version;
}
