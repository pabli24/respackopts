package io.gitlab.jfronny.respackopts.data.entry;

import io.gitlab.jfronny.respackopts.GuiFactory;
import io.gitlab.jfronny.respackopts.Respackopts;
import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.gui.entries.DropdownBoxEntry;
import me.shedaniel.clothconfig2.impl.builders.DropdownMenuBuilder;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class ConfigEnumEntry extends ConfigEntry<String> {
    public List<String> values = new ArrayList<>();
    private Integer nextValue;

    public void setNextValue(int v) {
        nextValue = v;
    }

    @Override
    public String getValue() {
        String v = super.getValue();
        if (v == null) {
            if (nextValue != null && nextValue >= 0 && nextValue < values.size()) {
                v = values.get(nextValue);
                setValue(v);
            }
            else {
                throw new NullPointerException("Could not get value");
            }
        }
        return v;
    }

    @Override
    public String getDefault() {
        String v = super.getDefault();
        if (v == null) {
            if (values.size() == 0)
                throw new NullPointerException("Could not get default entry as the entry array is empty");
            else
                v = values.get(0);
        }
        return v;
    }

    @Override
    public void sync(ConfigEntry<String> source, SyncMode mode) {
        super.sync(source, mode);
        ConfigEnumEntry n = (ConfigEnumEntry) source;
        if (mode == SyncMode.RESPACK_LOAD) {
            if (n.values != null && !n.values.isEmpty())
                values = n.values;
        }
        if (getValue() == null && nextValue != null) {
            if (n.nextValue >= 0 && n.nextValue < values.size()) {
                setValue(values.get(n.nextValue));
            }
            else {
                Respackopts.LOGGER.error("Could not load default value for enum");
            }
        }
    }

    @Override
    public void appendString(StringBuilder sb) {
        sb.append(getValue()).append('/').append(getValue()).append(" (").append(getDefault()).append('/').append(getDefault()).append(" of:");
        for (String e : values) {
            sb.append(' ').append(e);
        }
        sb.append(')');
    }

    @Override
    public ConfigEntry<String> clone() {
        ConfigEnumEntry e = new ConfigEnumEntry();
        e.nextValue = nextValue;
        e.values = List.copyOf(values);
        e.setValue(getValue());
        e.setDefault(getDefault());
        return e;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        sb.append("\n#define ");
        sb.append(valueName);
        sb.append(' ');
        sb.append(values.indexOf(getValue()));
        for (int i = 0; i < values.size(); i++) {
            String e2 = Respackopts.sanitizeString(values.get(i));
            if (version == 1) {
                sb.append("\n#define ");
                sb.append(valueName);
                sb.append('_');
                sb.append(e2);
                sb.append(' ');
                sb.append(i);
            } else {
                if (e2.equals(getValue())) {
                    sb.append("\n#define ");
                    sb.append(valueName);
                    sb.append('_');
                    sb.append(e2);
                }
            }
        }
    }

    @Override
    public AbstractConfigListEntry<?> buildEntry(ConfigEntryBuilder entryBuilder, Text name, Supplier<Optional<Text[]>> tooltipSupplier, String screenId, String entryName, String translationPrefix) {
        Function<String, Text> entryText = s -> GuiFactory.getText(s, ("".equals(translationPrefix) ? "" : translationPrefix + ".") + entryName);
        return entryBuilder.startDropdownMenu(name,
                DropdownMenuBuilder.TopCellElementBuilder.of(getValue(), s -> s, entryText),
                DropdownMenuBuilder.CellCreatorBuilder.of(entryText))
                .setSuggestionMode(false)
                .setDefaultValue(getDefault())
                .setSelections(() -> values.iterator())
                .setSaveConsumer(this::setValue)
                .setTooltipSupplier(tooltipSupplier)
                .build();
    }
}
