package io.gitlab.jfronny.respackopts.data.entry;

import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.text.Text;

import java.util.Optional;
import java.util.function.Supplier;

public class ConfigNumericEntry extends ConfigEntry<Double> {
    public Double min;
    public Double max;
    
    public ConfigNumericEntry() {
        setValue(0d);
    }

    @Override
    public void sync(ConfigEntry<Double> source, SyncMode mode) {
        super.sync(source, mode);
        ConfigNumericEntry n = (ConfigNumericEntry) source;
        if (mode == SyncMode.RESPACK_LOAD) {
            if (n.min != null)
                min = n.min;
            if (n.max != null)
                max = n.max;
        }
    }

    @Override
    public void appendString(StringBuilder sb) {
        sb.append(getValue())
                .append(" (")
                .append(getDefault())
                .append(", ")
                .append(min)
                .append("-")
                .append(max)
                .append(")");
    }

    @Override
    public ConfigEntry<Double> clone() {
        ConfigNumericEntry ce = new ConfigNumericEntry();
        ce.max = max;
        ce.min = min;
        ce.setValue(getValue());
        ce.setDefault(getDefault());
        return ce;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        sb.append("\n#define ");
        sb.append(valueName);
        sb.append(' ');
        sb.append(getValue().toString());
    }

    @Override
    public AbstractConfigListEntry<?> buildEntry(ConfigEntryBuilder entryBuilder, Text name, Supplier<Optional<Text[]>> tooltipSupplier, String screenId, String entryName, String translationPrefix) {
        if (min != null && max != null) {
            return entryBuilder.startIntSlider(name,
                    getValue().intValue(), min.intValue(), max.intValue())
                    .setDefaultValue(getDefault().intValue())
                    .setSaveConsumer(v -> setValue(v.doubleValue()))
                    .setTooltipSupplier(tooltipSupplier)
                    .build();
        }
        else {
            return entryBuilder.startDoubleField(name, getValue())
                    .setDefaultValue(getDefault())
                    .setSaveConsumer(this::setValue)
                    .setTooltipSupplier(tooltipSupplier)
                    .build();
        }
    }
}
