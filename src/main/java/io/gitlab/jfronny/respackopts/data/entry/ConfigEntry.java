package io.gitlab.jfronny.respackopts.data.entry;

import io.gitlab.jfronny.respackopts.Respackopts;
import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.text.Text;

import java.util.Optional;
import java.util.function.Supplier;

public abstract class ConfigEntry<T> {
    private T defaultValue;
    private T value;
    protected int version;

    public void setVersion(int version) {
        this.version = version;
    }
    
    public T getValue() {
        if (value == null) {
            if (defaultValue == null) {
                Respackopts.LOGGER.warn("No default value or current value set for entry, returning null");
                return null;
            }
            value = getDefault();
        }
        return value;
    }
    
    public void setValue(T value) {
        if (value != null)
            this.value = value;
    }
    
    public T getDefault() {
        if (defaultValue == null) {
            defaultValue = getValue();
            Respackopts.LOGGER.warn("No default value set for entry, using current");
        }
        return defaultValue;
    }
    
    public void setDefault(T value) {
        if (value != null)
            defaultValue = value;
    }
    
    public void sync(ConfigEntry<T> source, SyncMode mode) {
        if (mode == SyncMode.RESPACK_LOAD)
            setDefault(source.getDefault());
        if (mode == SyncMode.CONF_LOAD)
            setValue(source.getValue());
    }
    
    public void appendString(StringBuilder sb) {
        sb.append(value).append(" (").append(defaultValue).append(")");
    }

    @Override
    public String toString() {
        StringBuilder log = new StringBuilder();
        appendString(log);
        return log.toString();
    }
    
    public abstract ConfigEntry<T> clone();

    public abstract void buildShader(StringBuilder sb, String valueName);

    public abstract AbstractConfigListEntry<?> buildEntry(ConfigEntryBuilder entryBuilder, Text name, Supplier<Optional<Text[]>> tooltipSupplier, String screenId, String entryName, String translationPrefix);

    public String getEntryType() {
        return "field";
    }
}
