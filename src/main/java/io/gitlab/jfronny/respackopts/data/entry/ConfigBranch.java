package io.gitlab.jfronny.respackopts.data.entry;

import com.google.common.collect.ImmutableMap;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.RpoError;
import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import me.shedaniel.clothconfig2.impl.builders.SubCategoryBuilder;
import net.minecraft.text.Text;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class ConfigBranch extends ConfigEntry<Map<String, ConfigEntry<?>>> {
    public ConfigBranch() {
        setValue(new HashMap<>());
    }

    public boolean getBoolean(String name) throws RpoError {
        String[] sp = name.split("\\.");
        if (!super.getValue().containsKey(sp[0]))
            throw new RpoError("Invalid path to key");
        ConfigEntry<?> e = super.getValue().get(sp[0]);
        if (sp.length == 1) {
            if (e instanceof ConfigBooleanEntry b)
                return b.getValue();
            throw new RpoError("Not a boolean");
        }
        if (sp.length == 2 && e instanceof ConfigEnumEntry en) {
            for (String entry : en.values) {
                if (entry.equals(sp[1]))
                    return entry.equals(en.getValue());
            }
            throw new RpoError("Could not find enum entry");
        }
        if (e instanceof ConfigBranch b)
            return b.getBoolean(name.substring(name.indexOf('.') + 1));
        throw new RpoError("Invalid path to key");
    }

    @Override
    public void sync(ConfigEntry<Map<String, ConfigEntry<?>>> source, SyncMode mode) {
        for (Map.Entry<String, ConfigEntry<?>> e : source.getValue().entrySet()) {
            if (!has(e.getKey())) {
                if (mode == SyncMode.RESPACK_LOAD)
                    add(e.getKey(), e.getValue().clone());
            } else {
                ConfigEntry<?> current = get(e.getKey());
                if (e.getValue().getClass().equals(current.getClass())) {
                    syncSub(current, (ConfigEntry)e.getValue(), mode);
                }
                else {
                    Respackopts.LOGGER.warn("Type mismatch in config, ignoring");
                }
            }
        }
    }

    private <T> void syncSub(ConfigEntry<T> current, ConfigEntry<T> next, SyncMode mode) {
        current.sync(next, mode);
    }

    public <T> void add(String name, ConfigEntry<T> val) {
        val.setVersion(version);
        super.getValue().put(name, val);
    }
    
    public ConfigEntry<?> get(String key) {
        return super.getValue().get(key);
    }
    
    public boolean has(String key) {
        return super.getValue().containsKey(key);
    }

    @Override
    public Map<String, ConfigEntry<?>> getValue() {
        return ImmutableMap.copyOf(super.getValue());
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        for (Map.Entry<String, ConfigEntry<?>> e : super.getValue().entrySet()) {
            e.getValue().buildShader(sb, valueName + "_" + Respackopts.sanitizeString(e.getKey()));
        }
    }

    @Override
    public AbstractConfigListEntry<?> buildEntry(ConfigEntryBuilder entryBuilder, Text name, Supplier<Optional<Text[]>> tooltipSupplier, String screenId, String entryName, String translationPrefix) {
        SubCategoryBuilder sc = entryBuilder.startSubCategory(name);
        Respackopts.factory.buildCategory(this, screenId, sc::add, entryBuilder, entryName);
        sc.setTooltipSupplier(tooltipSupplier);
        return sc.build();
    }

    @Override
    public String getEntryType() {
        return "title";
    }

    @Override
    public void appendString(StringBuilder sb) {
        for (Map.Entry<String, ConfigEntry<?>> e : getValue().entrySet()) {
            sb.append("\n");
            sb.append(e.getKey());
            sb.append(": ");
            sb.append(e.getValue());
        }
    }

    @Override
    public ConfigEntry<Map<String, ConfigEntry<?>>> clone() {
        ConfigBranch branch = new ConfigBranch();
        for (Map.Entry<String, ConfigEntry<?>> e : getValue().entrySet()) {
            branch.add(e.getKey(), e.getValue().clone());
        }
        return branch;
    }

    @Override
    public void setVersion(int version) {
        super.setVersion(version);
        for (ConfigEntry<?> value : getValue().values()) {
            value.setVersion(version);
        }
    }
}
