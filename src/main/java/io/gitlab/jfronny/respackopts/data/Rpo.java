package io.gitlab.jfronny.respackopts.data;

import com.google.gson.JsonArray;

import java.util.List;

public class Rpo {
    public JsonArray conditions;
    public List<String> fallbacks;
}
