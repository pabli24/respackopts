package io.gitlab.jfronny.respackopts.data;

import java.util.HashMap;

public class LegacyConfig {
    public HashMap<String, Double> doubles = new HashMap<>();
    public HashMap<String, Boolean> bools = new HashMap<>();
}
