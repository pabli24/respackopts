package io.gitlab.jfronny.respackopts.data.entry;

import me.shedaniel.clothconfig2.api.AbstractConfigListEntry;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.text.Text;

import java.util.Optional;
import java.util.function.Supplier;

public class ConfigBooleanEntry extends ConfigEntry<Boolean> {
    public ConfigBooleanEntry(boolean v) {
        setValue(v);
    }

    @Override
    public ConfigEntry<Boolean> clone() {
        ConfigBooleanEntry be = new ConfigBooleanEntry(getValue());
        be.setDefault(getDefault());
        return be;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        if (getValue()) {
            sb.append("\n#define ");
            sb.append(valueName);
        }
    }

    @Override
    public AbstractConfigListEntry<?> buildEntry(ConfigEntryBuilder entryBuilder, Text name, Supplier<Optional<Text[]>> tooltipSupplier, String screenId, String entryName, String translationPrefix) {
        return entryBuilder.startBooleanToggle(name, getValue())
                .setDefaultValue(getDefault())
                .setSaveConsumer(this::setValue)
                .setTooltipSupplier(tooltipSupplier)
                .build();
    }
}
