package io.gitlab.jfronny.respackopts.filters.conditions;

import com.google.gson.JsonElement;
import io.gitlab.jfronny.respackopts.data.RpoError;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

public class EqualityCondition implements Condition {
    @Override
    public boolean evaluate(JsonElement node) throws RpoError {
        if (!node.isJsonArray())
            throw new RpoError("\"equal\" condition requires an array of conditions");
        Optional<Boolean> v = Optional.empty();
        for (JsonElement jsonElement : node.getAsJsonArray()) {
            boolean current = ConditionEvaluator.evaluate(jsonElement);
            if (v.isEmpty())
                v = Optional.of(current);
            if (current != v.get())
                return false;
        }
        return true;
    }

    @Override
    public Set<String> getKeys() {
        Set<String> strings = new LinkedHashSet<>();
        strings.add("==");
        strings.add("=");
        strings.add("equal");
        strings.add("eq");
        return strings;
    }
}
