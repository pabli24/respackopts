package io.gitlab.jfronny.respackopts.filters;

import io.gitlab.jfronny.respackopts.filters.conditions.ResourcePackFilter;
import io.gitlab.jfronny.respackopts.filters.fallback.FallbackFilter;
import net.minecraft.resource.ResourceNotFoundException;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.function.Predicate;

public class FilterProvider {
    ResourcePackFilter rpo;
    FallbackFilter fbt;
    Predicate<String> containsFileBase;
    boolean containsFileWasFallback = false;
    public FilterProvider(Predicate<String> containsFileBase, FileOpenProvider openFileBase) {
        this.containsFileBase = containsFileBase;
        rpo = new ResourcePackFilter(containsFileBase, openFileBase);
        fbt = new FallbackFilter(containsFileBase, openFileBase);
    }

    public void openFile(String name, File base, CallbackInfoReturnable<InputStream> info) throws IOException {
        if (containsFileBase.test(name) && containsFileWasFallback) {
            info.setReturnValue(fbt.getReplacement(name, new ResourceNotFoundException(base, name)));
        }
    }

    public void containsFile(String name, CallbackInfoReturnable<Boolean> info) {
        containsFileWasFallback = false;
        if (info.getReturnValueZ()) {
            if (rpo.fileHidden(name)) {
                if (fbt.fileVisible(name)) {
                    containsFileWasFallback = true;
                } else {
                    info.setReturnValue(false);
                }
            }
        }
        else {
            if (fbt.fileVisible(name)) {
                containsFileWasFallback = true;
                info.setReturnValue(true);
            }
        }
    }

    public void findResources(String namespace, CallbackInfoReturnable<Collection<Identifier>> info) {
        Collection<Identifier> ret = info.getReturnValue();
        ret.removeIf(s -> rpo.fileHidden(s.getPath()) && !fbt.fileVisible(namespace));
        fbt.addFallbackResources(ret, namespace);
    }
}
