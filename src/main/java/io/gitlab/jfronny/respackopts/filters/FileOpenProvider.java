package io.gitlab.jfronny.respackopts.filters;

import java.io.IOException;
import java.io.InputStream;

public interface FileOpenProvider {
    InputStream open(String file) throws IOException;
}
