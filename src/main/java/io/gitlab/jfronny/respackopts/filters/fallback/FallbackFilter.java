package io.gitlab.jfronny.respackopts.filters.fallback;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.Rpo;
import io.gitlab.jfronny.respackopts.filters.FileOpenProvider;
import net.minecraft.resource.ResourceNotFoundException;
import net.minecraft.util.Identifier;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class FallbackFilter {
    Predicate<String> containsFileBase;
    FileOpenProvider openFileBase;
    public FallbackFilter(Predicate<String> containsFileBase, FileOpenProvider openFileBase) {
        this.containsFileBase = containsFileBase;
        this.openFileBase = openFileBase;
    }

    public boolean fileVisible(String name) {
        if (name.endsWith(Respackopts.FILE_EXTENSION))
            return false;
        String fbt = name + Respackopts.FILE_EXTENSION;
        if (containsFileBase.test(fbt)) {
            try (InputStream stream = openFileBase.open(fbt); Reader w = new InputStreamReader(stream)) {
                Rpo rpo = Respackopts.GSON.fromJson(w, Rpo.class);
                if (rpo.fallbacks != null) {
                    List<String> arr = rpo.fallbacks;
                    for (String s : arr) {
                        if (containsFileBase.test(s))
                            return true;
                    }
                }
            }
            catch (IOException e) {
                Respackopts.LOGGER.error("Could not determine visibility of " + name + e);
            }
        }
        return false;
    }

    public InputStream getReplacement(String name, ResourceNotFoundException ex) throws ResourceNotFoundException {
        String fbt = name + Respackopts.FILE_EXTENSION;
        try (InputStream stream = openFileBase.open(fbt); Reader w = new InputStreamReader(stream)) {
            Rpo rpo = Respackopts.GSON.fromJson(w, Rpo.class);
            if (rpo.fallbacks != null) {
                List<String> arr = rpo.fallbacks;
                for (String s : arr) {
                    if (containsFileBase.test(s))
                        return openFileBase.open(s);
                }
            }
        }
        catch (IOException e) {
            Respackopts.LOGGER.error("Could not determine replacement for " + name, e);
        }
        throw ex;
    }

    public void addFallbackResources(Collection<Identifier> ret, String namespace) {
        for (Identifier identifier : ret) {
            String path = identifier.getPath();
            if (path.endsWith(Respackopts.FILE_EXTENSION)) {
                String expectedTarget = path.substring(0, path.length() - Respackopts.FILE_EXTENSION.length());
                if (ret.stream().noneMatch(s -> s.getPath().equals(expectedTarget)) && fileVisible(expectedTarget)) {
                    ret.add(new Identifier(namespace, expectedTarget));
                }
            }
        }
    }
}
