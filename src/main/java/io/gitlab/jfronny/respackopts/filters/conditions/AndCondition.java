package io.gitlab.jfronny.respackopts.filters.conditions;

import com.google.gson.JsonElement;
import io.gitlab.jfronny.respackopts.data.RpoError;

import java.util.LinkedHashSet;
import java.util.Set;

public class AndCondition implements Condition {
    @Override
    public boolean evaluate(JsonElement node) throws RpoError {
        if (!node.isJsonArray())
            throw new RpoError("\"and\" condition requires an array of conditions");
        for (JsonElement jsonElement : node.getAsJsonArray()) {
            if (!ConditionEvaluator.evaluate(jsonElement))
                return false;
        }
        return true;
    }

    @Override
    public Set<String> getKeys() {
        Set<String> strings = new LinkedHashSet<>();
        strings.add("add");
        strings.add("&");
        return strings;
    }
}
