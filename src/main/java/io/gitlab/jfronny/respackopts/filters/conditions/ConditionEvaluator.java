package io.gitlab.jfronny.respackopts.filters.conditions;

import com.google.gson.JsonElement;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.RpoError;
import io.gitlab.jfronny.respackopts.data.entry.ConfigBranch;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class ConditionEvaluator {
    private static final Set<Condition> conditions;
    static {
        conditions = new LinkedHashSet<>();
        conditions.add(new AndCondition());
        conditions.add(new OrCondition());
        conditions.add(new XorCondition());
        conditions.add(new EqualityCondition());
        conditions.add(new NorCondition());
    }

    public static boolean evaluate(JsonElement condition) throws RpoError {
        if (condition.isJsonPrimitive() && condition.getAsJsonPrimitive().isString())
            return evaluate(condition.getAsString());
        if (condition.isJsonObject() && condition.getAsJsonObject().size() == 1) {
            for (Map.Entry<String, JsonElement> entry : condition.getAsJsonObject().entrySet()) {
                for (Condition c : conditions) {
                    if (c.getKeys().contains(entry.getKey())) {
                        return c.evaluate(entry.getValue());
                    }
                }
                throw new RpoError("Could not find condition: " + entry.getKey());
            }
        }
        if (condition.isJsonArray()) {
            for (JsonElement element : condition.getAsJsonArray()) {
                if (!evaluate(element))
                    return false;
            }
            return true;
        }
        throw new RpoError("Condition entries may only be json objects containing one key and one value or strings");
    }

    public static boolean evaluate(String condition) throws RpoError {
        if (condition == null) {
            throw new RpoError("Condition must not be null");
        }
        if (!condition.contains(":")) {
            throw new RpoError("You must include you resource pack ID in conditions (format: pack:some.key)");
        }

        String sourcePack = condition.split(":")[0];
        String name = condition.substring(condition.indexOf(':') + 1);
        for (Map.Entry<String, ConfigBranch> e : Respackopts.CONFIG_BRANCH.entrySet()) {
            if (Objects.equals(e.getKey(), sourcePack)) {
                return e.getValue().getBoolean(name);
            }
        }
        throw new RpoError("Could not find pack with specified ID");
    }
}
