package io.gitlab.jfronny.respackopts.filters.conditions;

import com.google.gson.JsonElement;
import io.gitlab.jfronny.respackopts.data.RpoError;

import java.util.Set;

public interface Condition {
    boolean evaluate(JsonElement node) throws RpoError;
    Set<String> getKeys();
}
