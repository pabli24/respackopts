package io.gitlab.jfronny.respackopts.filters.conditions;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.data.Rpo;
import io.gitlab.jfronny.respackopts.filters.FileOpenProvider;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.Predicate;

public class ResourcePackFilter {
    Predicate<String> containsFileBase;
    FileOpenProvider openFileBase;
    public ResourcePackFilter(Predicate<String> containsFileBase, FileOpenProvider openFileBase) {
        this.containsFileBase = containsFileBase;
        this.openFileBase = openFileBase;
    }

    public boolean fileHidden(String name) {
        if (name.endsWith(Respackopts.FILE_EXTENSION))
            return false;
        if (!containsFileBase.test(name + Respackopts.FILE_EXTENSION))
            return false;
        try (InputStream stream = openFileBase.open(name + Respackopts.FILE_EXTENSION); Reader w = new InputStreamReader(stream)) {
            Rpo rpo = Respackopts.GSON.fromJson(w, Rpo.class);
            if (rpo.conditions == null)
                return false;
            return !ConditionEvaluator.evaluate(rpo.conditions);
        }
        catch (Throwable e) {
            Respackopts.LOGGER.error("Could not load RPO file " + name, e);
            return false;
        }
    }
}
