package io.gitlab.jfronny.respackopts.filters.conditions;

import com.google.gson.JsonElement;
import io.gitlab.jfronny.respackopts.data.RpoError;

import java.util.LinkedHashSet;
import java.util.Set;

public class XorCondition implements Condition {
    @Override
    public boolean evaluate(JsonElement node) throws RpoError {
        if (!node.isJsonArray())
            throw new RpoError("\"xor\" condition requires an array of conditions");
        boolean bl = false;
        for (JsonElement jsonElement : node.getAsJsonArray()) {
            if (ConditionEvaluator.evaluate(jsonElement))
                bl = !bl;
        }
        return bl;
    }

    @Override
    public Set<String> getKeys() {
        Set<String> strings = new LinkedHashSet<>();
        strings.add("^");
        strings.add("xor");
        return strings;
    }
}
