/*******************************************************
 *  lumi:shaders/internal/debug_shading.glsl           *
 *******************************************************
 *  Copyright (c) 2020 spiralhalo and Contributors.    *
 *  Released WITHOUT WARRANTY under the terms of the   *
 *  GNU Lesser General Public License version 3 as     *
 *  published by the Free Software Foundation, Inc.    *
 *******************************************************/

#if lumi_debugMode != lumi_debugMode_none
void debug_shading(inout vec4 a) {
#if lumi_debugMode == lumi_debugMode_viewDir
    vec3 viewDir = normalize(-pbrv_viewPos) * frx_normalModelMatrix() * gl_NormalMatrix;
    a.rgb = viewDir * 0.5 + 0.5;
#else
    vec3 normal = fragData.vertexNormal * frx_normalModelMatrix();
    a.rgb = normal * 0.5 + 0.5;
#endif
}
#endif
