/***********************************************************
 *  lumi:shaders/internal/tonemap.glsl                     *
 ***********************************************************/

vec3 hdr_reinhardJodieTonemap(in vec3 v) {
    float l = frx_luminance(v);
    vec3 tv = v / (1.0f + v);
    return mix(v / (1.0f + l), tv, tv);
}

#if lumi_tonemap == lumi_tonemap_vibrant
vec3 hdr_vibrantTonemap(in vec3 hdrColor){
	return hdrColor / (frx_luminance(hdrColor) + vec3(1.0));
}
#endif

void tonemap(inout vec4 a) {
#if lumi_tonemap == lumi_tonemap_film
    a.rgb = frx_toGamma(frx_toneMap(a.rgb));
#elif lumi_tonemap == lumi_tonemap_vibrant
    a.rgb = pow(hdr_vibrantTonemap(a.rgb), vec3(1.0 / hdr_gamma));
#else
    a.rgb = pow(hdr_reinhardJodieTonemap(a.rgb), vec3(1.0 / hdr_gamma));
#endif
}
